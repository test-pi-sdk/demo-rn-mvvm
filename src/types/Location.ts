export type Location = {
  accuracy: number
  altitude: number | null
  altitudeAccuracy: number | null
  heading: number | null
  speed: number | null
  latitude: number
  longitude: number
}
