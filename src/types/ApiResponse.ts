export type Response = {
  destination_addresses: Array<string>
  origin_addresses: Array<string>
  rows: Array<Element>
}

export type Element = {
  elements: Array<ElementDetail>
}

export type ElementDetail = {
  distance: {
    text: string
    value: number
  }
  duration: {
    text: string
    value: number
  }
  duration_in_traffic: {
    text: string
    value: number
  }
  status: string
}
