export type Order = {
  id: number
  name: string
  rating: number
  categories: number[]
  priceRating: number
  photo: any
  duration: string
  location: {
    latitude: number
    longitude: number
  }
  courier: {
    avatar: any
    name: string
  }
  menu: Menu[]
}

export type Menu = {
  menuId: number
  name: string
  photo: any
  description: string
  calories: number
  price: number
}
