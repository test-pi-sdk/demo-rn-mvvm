import React from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'
import MapViewDirections from 'react-native-maps-directions'
import { icons, COLORS, SIZES, GOOGLE_API_KEY } from '../../../constants'
import useRenderMapViewModel from './view.model'

const RenderMap = () => {
  const {
    mapView,
    angle,
    fromLocation,
    toLocation,
    region,
    isReady,
    setFromLocation,
    setDuration,
    setAngle,
    setIsReady,
    calculateAngle
  } = useRenderMapViewModel()
  const destinationMarker = () => (
    <Marker coordinate={toLocation}>
      <View
        style={{
          height: 40,
          width: 40,
          borderRadius: 20,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: COLORS.white
        }}
      >
        <View
          style={{
            height: 30,
            width: 30,
            borderRadius: 15,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: COLORS.primary
          }}
        >
          <Image
            source={icons.pin}
            style={{
              width: 25,
              height: 25,
              tintColor: COLORS.white
            }}
          />
        </View>
      </View>
    </Marker>
  )

  const carIcon = () => (
    <Marker
      coordinate={fromLocation}
      anchor={{ x: 0.5, y: 0.5 }}
      flat={true}
      rotation={angle}
    >
      <Image
        source={icons.car}
        style={{
          width: 40,
          height: 40
        }}
      />
    </Marker>
  )

  return (
    <View style={{ flex: 1 }}>
      <MapView
        ref={mapView}
        provider={PROVIDER_GOOGLE}
        initialRegion={region}
        style={{ flex: 1 }}
      >
        <MapViewDirections
          origin={fromLocation}
          destination={toLocation}
          apikey={GOOGLE_API_KEY}
          strokeWidth={5}
          strokeColor={COLORS.primary}
          optimizeWaypoints={true}
          onReady={(result) => {
            setDuration(result.duration)

            if (!isReady) {
              // Fit route into maps
              mapView.current.fitToCoordinates(result.coordinates, {
                edgePadding: {
                  right: SIZES.width / 20,
                  bottom: SIZES.height / 4,
                  left: SIZES.width / 20,
                  top: SIZES.height / 8
                }
              })

              // Reposition the car
              let nextLoc = {
                latitude: result.coordinates[0]['latitude'],
                longitude: result.coordinates[0]['longitude']
              }

              if (result.coordinates.length >= 2) {
                let angle = calculateAngle(result.coordinates)
                setAngle(angle)
              }

              setFromLocation(nextLoc)
              setIsReady(true)
            }
          }}
        />
        {destinationMarker()}
        {fromLocation ? carIcon() : null}
      </MapView>
    </View>
  )
}

export default RenderMap
