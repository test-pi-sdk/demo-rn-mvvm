import { Coordinates } from '../../types/Coordinates'
import { MapRegion } from '../../types/MapRegion'

export interface RenderMapViewModel {
  mapView: any
  fromLocation: Coordinates
  toLocation: Coordinates
  region: MapRegion
  angle: number
  duration: number
  isReady: boolean
  setIsReady: React.Dispatch<React.SetStateAction<boolean>>
  setAngle: React.Dispatch<React.SetStateAction<number>>
  setFromLocation: React.Dispatch<React.SetStateAction<Coordinates>>
  setDuration: React.Dispatch<React.SetStateAction<number>>
  calculateAngle: ({ coordinates }: any) => number
  zoomIn: () => void
  zoomOut: () => void
}
