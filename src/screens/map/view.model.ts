import { useRef, useState, useEffect } from 'react'
import { Coordinates } from '../../types/Coordinates'
import useHeaderViewModel from '../header/view.model'
import { RenderMapViewModel } from './models'
// import { Coordinates } from '../../types/Coordinates'

const useRenderMapViewModel = (): RenderMapViewModel => {
  const { location } = useHeaderViewModel()
  const mapView = useRef<any>()
  const [restaurant, setRestaurant] = useState()
  const [streetName, setStreetName] = useState('')
  const [fromLocation, setFromLocation] = useState<Coordinates>({
    latitude: location?.latitude || 0,
    longitude: location?.longitude || 0
  })
  const [toLocation, setToLocation] = useState<Coordinates>({
    latitude: 0,
    longitude: 0
  })
  const [region, setRegion] = useState<any>()

  const [duration, setDuration] = useState(0)
  const [isReady, setIsReady] = useState<boolean>(false)
  const [angle, setAngle] = useState<number>(0)

  const calculateAngle = ({ coordinates }: any) => {
    let startLat = coordinates[0]['latitude']
    let startLng = coordinates[0]['longitude']
    let endLat = coordinates[1]['latitude']
    let endLng = coordinates[1]['longitude']
    let dx = endLat - startLat
    let dy = endLng - startLng

    return (Math.atan2(dy, dx) * 180) / Math.PI
  }
  const zoomIn = () => {
    let newRegion = {
      latitude: region.latitude,
      longitude: region.longitude,
      latitudeDelta: region.latitudeDelta / 2,
      longitudeDelta: region.longitudeDelta / 2
    }
    setRegion(newRegion)
    mapView.current.animateToRegion(newRegion, 200)
  }

  const zoomOut = () => {
    let newRegion = {
      latitude: region.latitude,
      longitude: region.longitude,
      latitudeDelta: region.latitudeDelta * 2,
      longitudeDelta: region.longitudeDelta * 2
    }
    setRegion(newRegion)
    mapView.current.animateToRegion(newRegion, 200)
  }
  useEffect(() => {
    //  let { restaurant, currentLocation } = route.params
    //  let fromLoc = currentLocation.gps
    //  let toLoc = restaurant.location
    //  let street = currentLocation.streetName
    //  let mapRegion = {
    //    latitude: (fromLoc.latitude + toLoc.latitude) / 2,
    //    longitude: (fromLoc.longitude + toLoc.longitude) / 2,
    //    latitudeDelta: Math.abs(fromLoc.latitude - toLoc.latitude) * 2,
    //    longitudeDelta: Math.abs(fromLoc.longitude - toLoc.longitude) * 2
    //  }
    //  setRestaurant(restaurant)
    //  setStreetName(street)
    //  setFromLocation(fromLoc)
    //  setToLocation(toLoc)
    //  setRegion(mapRegion)
  }, [])

  return {
    mapView,
    angle,
    fromLocation,
    toLocation,
    region,
    isReady,
    duration,
    setFromLocation,
    setDuration,
    setAngle,
    setIsReady,
    calculateAngle,
    zoomIn,
    zoomOut
  }
}
export default useRenderMapViewModel
