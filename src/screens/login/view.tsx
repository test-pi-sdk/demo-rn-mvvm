import { StatusBar } from 'expo-status-bar'
import React from 'react'
import {
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Text
} from 'react-native'
import useLoginViewModel from './view.model'
import { icons, COLORS } from '../../../constants'

const LoginView: React.FC = () => {
  const { setEmail, setPassword, onSubmit } = useLoginViewModel()

  return (
    <View style={styles.container}>
      {/* <View style={styles.image}>
        <img src={icons.car} alt='Pomas' style={{ borderRadius: '50%' }} />
      </View> */}
      <StatusBar style='auto' />
      <Text style={styles.header}>LOGIN</Text>
      <View>
        <TextInput
          style={styles.TextInput}
          placeholder='Email'
          placeholderTextColor='#003f5c'
          onChangeText={(email) => setEmail(email)}
        />
      </View>
      <View>
        <TextInput
          style={styles.TextInput}
          placeholder='Password'
          placeholderTextColor='#003f5c'
          secureTextEntry={true}
          onChangeText={(password) => setPassword(password)}
        />
      </View>
      <TouchableOpacity style={styles.loginBtn} onPress={onSubmit}>
        <Text>LOGIN</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Text style={styles.forgot_button}>Forgot Password?</Text>
      </TouchableOpacity>
    </View>
  )
}

export default LoginView

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    width: '50px',
    height: '50px',
    borderRadius: 50
  },
  header: {
    fontWeight: 'bold',
    marginBottom: 25,
    fontSize: 30
  },
  TextInput: {
    backgroundColor: COLORS.primary,
    borderRadius: 30,
    width: '100%',
    marginBottom: 20,
    alignItems: 'center',
    flex: 1,
    padding: 10
  },
  loginBtn: {
    width: '179px',
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    backgroundColor: COLORS.primary
  },
  forgot_button: {
    height: 30,
    marginTop: 10
  },
  loginText: {}
})
