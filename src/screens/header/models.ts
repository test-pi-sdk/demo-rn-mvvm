import { Location } from '../../types/Location'

export interface HeaderViewModel {
  onLogin: () => void
  initialCurrentLocation: {
    streetName: string
    gps: {
      latitude: number
      longitude: number
    }
  }
  location: Location | undefined
  requestLocationPermission: () => Promise<void>
}
