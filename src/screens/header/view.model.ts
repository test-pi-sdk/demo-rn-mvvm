import { HeaderViewModel } from './models'
import { useNavigation } from '@react-navigation/native'
import Geolocation from '@react-native-community/geolocation'
import { useEffect, useState } from 'react'
import { PermissionsAndroid } from 'react-native'
import { Location } from '../../types/Location'

const useHeaderViewModel = (): HeaderViewModel => {
  const [location, setLocation] = useState<Location | undefined>()
  useEffect(() => {
    requestLocationPermission()
  }, [])

  const requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permission',
          message: 'This app requires access to your location',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK'
        }
      )
      if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
        Geolocation.getCurrentPosition(
          (position) => setLocation(position.coords),
          (error) => console.log(error),
          { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        )
      }
    } catch (err) {
      console.warn(err)
    }
  }

  const navigation = useNavigation()
  const initialCurrentLocation = {
    streetName: 'Viet Nam',
    gps: {
      latitude: 1.5496614931250685,
      longitude: 110.36381866919922
    }
  }
  const onLogin = async () => {
    navigation.navigate('Login' as never)
  }

  return {
    initialCurrentLocation,
    onLogin,
    location,
    requestLocationPermission
  }
}
export default useHeaderViewModel
