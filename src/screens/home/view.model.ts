import { useState } from 'react'
import { HomeViewModel } from './models'

const useHomeViewModel = (): HomeViewModel => {
  const [id, setId] = useState<number>(0)
  const [name, setName] = useState<string>('')
  const [icon, setIcon] = useState<string>('')

  const [address, setAddress] = useState<string>('')
  return {
    id,
    setId,
    name,
    setName,
    icon,
    setIcon,
    address,
    setAddress
  }
}
export default useHomeViewModel
