export interface HomeViewModel {
  id: number
  name: string
  icon: string
  address: string
  setId: React.Dispatch<React.SetStateAction<number>>
  setName: React.Dispatch<React.SetStateAction<string>>
  setIcon: React.Dispatch<React.SetStateAction<string>>
  setAddress: React.Dispatch<React.SetStateAction<string>>
}
