import React from 'react'
import useHomeViewModel from './view.model'
import Header from '../header/view'
import OrderList from '../order-list/view'

const HomeView: React.FC<any> = ({ navigation }: any) => {
  const { id, name, address } = useHomeViewModel()

  return (
    <>
      <Header />
      <OrderList />
    </>
  )
}

export default HomeView
