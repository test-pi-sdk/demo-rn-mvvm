import React from 'react'
// import { createStackNavigator } from '@react-navigation/stack'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { NavigationContainer } from '@react-navigation/native'
import { HomeView, LoginView } from './src/screens'
import { Text } from 'react-native'

const Stack = createNativeStackNavigator()

const App: React.FC = () => {
  return (
    // <AppNavigator />
    <NavigationContainer fallback={<Text>Loading...</Text>}>
      <Stack.Navigator
        screenOptions={{
          headerShown: false
        }}
        initialRouteName='Home'
      >
        <Stack.Screen name='Home' component={HomeView} />
        <Stack.Screen
          name='Login'
          component={LoginView}
          options={{ title: 'Overview' }}
        />
        {/* <Stack.Screen name='OrderDelivery' component={OrderDelivery} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App
